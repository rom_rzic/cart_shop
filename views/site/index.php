<?php foreach ($products as $product) :?>
<article class="col-sm-3 product">
    <h3><?=$product->name;?></h3>
    <div class="description"><?=$product->description;?></div>
    <div class="price">Цена: <?=$product->price;?></div>


        <input type="button" class="less count_operation" value="-">
        <input type="text" value='1' class="quantity"/>
        <input type="button" class="more count_operation" value="+">

    <input type="hidden" class="id" value="<?=$product->id;?>"/>
    <input type="button" class='addProduct' value="Добавить в корзину"/>
    <?php if(!empty($_SESSION['cart'][$product->id])) :?>
        <span class="del"><input type="button" class='deleteProduct' value="Удалить из корзины"/></span>
    <?php endif;?>
</article>
<?php endforeach;?>
