<?php
use yii\helpers\Html;
use yii\helpers\Url;

app\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DОСТРЕ html>
<html lang="<?= Yii::$app->language?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body>
<?php $this->beginBody() ?>
<nav class="navbar navbar-default">
    <div class="container">
            <ul class="nav navbar-nav navbar-right  ">
                  <li><a href="<?= Url::toRoute(['site/cart'])?>">Корзина</a></li>
            </ul>
    </div>
</nav>
<div class="container">
    <?= $content ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>