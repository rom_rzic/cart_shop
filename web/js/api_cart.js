$(".addProduct").on('click', function () {
    id = $(this).prevAll('.id').val();
    quantity = $(this).prevAll('.quantity').val();
    button = $(this);
    console.log(quantity);
    $.ajax({
        url: '/api/add-to-cart',
        type: 'post',
        data: {'id' : id, 'quantity': quantity},
        success: function(res){
            div = $('<button/>', {
                'class' : 'deleteProduct',
                'text'  : 'Удалить из корзины',
                'click'   : function () {
                    deleteProduct($(this));
                }
            });
            button.next('.del').html(div);
        },
        error:function(jqXHR, textStatus, errorThrown) {
            if(errorThrown === 'Bad Request')
                alert('Неверный запрос.');
            console.log(jqXHR);
        }
    });
});

$(".deleteProduct").on('click', function () {
    deleteProduct($(this));
});
function deleteProduct(button) {
    id = button.parent().prevAll('.id').val();
    quantity = button.parent().prevAll('.quantity').val();
    console.log(id);
    $.ajax({
        url: '/api/delete-to-cart',
        type: 'post',
        data: {'id' : id, 'quantity': quantity},
        success: function(res){
            result = JSON.parse(res);
            if(result.success === 'end')
                button.remove();
        },
        error:function(jqXHR, textStatus, errorThrown) {
            if(errorThrown === 'Bad Request')
                alert('Неверный запрос.');
            console.log(jqXHR);
        }
    });
}

$('.quantity').on('keydown', function (e) {
    e.preventDefault();
});

$('.less').on('click', function () {
    quantity =  $(this).next('.quantity').val();
    quantity = Number(quantity) - 1;
    quantity = validQuantity(quantity);
   $(this).next('.quantity').val(quantity);
});

$('.more').on('click', function () {
    quantity = $(this).prev('.quantity').val();
    quantity = Number(quantity) + 1;
    quantity = validQuantity(quantity);
    $(this).prev('.quantity').val(quantity);
});

function validQuantity(quantity) {
    if(quantity <= 0 )
        quantity = 1;
    if(quantity > 10)
        quantity = 10;
    return quantity;
}