<?php

namespace app\controllers;


use app\models\Product;
use Yii;
use yii\web\Controller;


class SiteController extends Controller
{
    public function actionIndex()
    {
        Yii::$app->session->open();
        $products = Product::find()->all();
        return $this->render('index', compact('products'));
    }

    public function actionError(){
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            $name = $exception->getName();
            $message = $exception->getMessage();
            $status = $exception->statusCode;
            return $this->render('error', compact('name', 'message', 'status'));
        }
    }
}
