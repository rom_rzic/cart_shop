<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.10.2017
 * Time: 14:59
 */
namespace app\controllers;


use app\models\Cart;
use app\models\Product;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;

class ApiController extends Controller
{
    public function actionAddToCart(){
        $id = Yii::$app->request->post('id');
        $quantity = Yii::$app->request->post('quantity');


        if(!preg_match('/^\d+$/', $id))
            throw new HttpException(400, 'Invalid data parameters');
        if(!preg_match('/^\d+$/', $quantity))
            throw new HttpException(400, 'Invalid data parameters');
        $cart = new Cart();
        $cart->addToCart($id, $quantity);

    }

    public function actionDeleteToCart(){
        $id = Yii::$app->request->post('id');
        $quantity = Yii::$app->request->post('quantity');


        if(!preg_match('/^\d+$/', $id))
            throw new HttpException(400, 'Invalid data parameters');
        if(!preg_match('/^\d+$/', $quantity))
            throw new HttpException(400, 'Invalid data parameters');


        $cart = new Cart();
        $res = $cart->deleteToCart($id, $quantity);
        return json_encode(['success' => $res]);
    }

    public function actionCart(){



        $cart = new Cart();
        if(empty($_SESSION['cart']))
            return 'Корзина пуста';
        $arProduct = $cart->cart();

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $arProduct;
        return $response;
    }

    public function actionProducts(){
        $cart = new Cart();
        $arProduct = $cart->product();

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $arProduct;
        return $response;
    }
}