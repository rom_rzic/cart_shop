<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08.10.2017
 * Time: 0:09
 */

namespace app\models;


use Yii;
use yii\web\HttpException;

class Cart
{
    public function __construct()
    {
        Yii::$app->session->open();
    }

    public function addToCart($id, $quantity){
        $product = Product::find()->where(['id' => $id])->one();

        if(!$product)
            throw new HttpException(400, 'Invalid data parameters');

        if(isset($_SESSION['cart'][$id])) {
            $_SESSION['cart'][$id]['quantity'] += $quantity;
            $_SESSION['cart'][$id]['sum'] = $product->price * $_SESSION['cart'][$id]['quantity'];
        }
        else {
            $_SESSION['cart'][$id]['quantity'] = $quantity;
            $_SESSION['cart'][$id]['sum'] = $product->price * $quantity;
            $_SESSION['cart'][$id]['product_id'] = $id;
        }
    }

    public function deleteToCart($id, $quantity){

        if(isset($_SESSION['cart'][$id])) {
            $_SESSION['cart'][$id]['quantity'] -= $quantity;
        }elseif(!isset($_SESSION['cart'][$id])){
            throw new HttpException(400, 'Invalid data parameters');
        }
        if($_SESSION['cart'][$id]['quantity'] <= 0) {
            unset($_SESSION['cart'][$id]);
            return 'end';
        }
    }

    public function cart(){
        $arProduct = [];
        $total_sum = 0;

        foreach ($_SESSION['cart'] as $product){
            $arProduct['data']['product'][] = $product;
            $total_sum += $product['sum'];
        }
        $arProduct['data']['total_sum'] = $total_sum;
        $arProduct['data']['products_count'] = count($arProduct['data']['product']);

        return $arProduct;
    }

    public function product(){
        $result = Product::find()->all();
        $arProduct = [];
        foreach ($result as $key => $product){
            $arProduct['data'][$key]['id'] = $product->id;
            $arProduct['data'][$key]['name'] = $product->name;
            $arProduct['data'][$key]['description'] = $product->description;
            $arProduct['data'][$key]['price'] = $product->price;
        }
        return $arProduct;
    }

    public function __destruct()
    {
        Yii::$app->session->close();
    }
}