<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m171007_120525_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name'          => $this->string(),
            'description'    => $this->text(),
            'price'         => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
