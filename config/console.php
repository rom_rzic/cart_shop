<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02.10.2017
 * Time: 13:05
 */
return [

    'id'            => 'product',
    'basePath'      => dirname( __DIR__),
    'components'    => [
        'db'    => require(__DIR__ . '/db.php'),
    ],
];